module.exports = function(grunt){

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		paths:{
			source:'./src/',
			dev:'./dev/',
			build:'./build/',
		},
		syncDevFiles:['*.html', '**/models/**'],
		syncBuildFiles:['*.html'],
		sync:{
			dev:{
				files:[
					{
						cwd:'<%= paths.source %>',
						src:'<%= syncDevFiles %>',
						dest: '<%= paths.dev %>',
					}
				],
				verbose:true,
				updateOnly:true
			},
			build:{
				files:[
					{
						cwd:'<%= paths.source %>',
						src:'<%= syncBuildFiles %>',
						dest: '<%= paths.dev %>',
					}
				],
				verbose:true,
				updateOnly:true
			}
		},
		watch:{
			files:['<%= paths.source %>/**/*'],
			tasks:['sass:dev', 'sync:dev']
		},
		browserSync:{
			options:{
				watchTask:true,
				server:'<%= paths.dev %>'
			},
			default_options:
			{
				bsFiles:{
					src:['<%= paths.dev %>**/*'],
				}
			}
		},
		sass:
		{
			dev:
			{
				files:
				{
					'<%= paths.dev %>css/main.css':'<%= paths.source %>scss/main.scss'
				}
			},
			build:
			{
				options:
				{
					style:"compressed",
					sourcemap:"none"
				},
				files:
				{
					'<%= paths.build %>css/main.css':'<%= paths.source %>scss/main.scss'
				}
			}
		},
		browserify: {
			options: {
				transform: [['babelify', {
					presets: [["@babel/preset-env"]] 
				}]]
			},
			dev: {
				options: {
					watch: true,
					keepAlive: false,
					browserifyOptions: {
						debug: true
					}
				},
				files: {
					'<%= paths.dev %>js/main.js': ['<%= paths.source %>js/main.js'],
				}
			},
			build: {
				options: {
					debug: false
				},
				files: {
					'<%= paths.build %>js/main.js': '<%= paths.source %>js/main.js',
				}
			}
		},
	}); 

	grunt.loadNpmTasks('grunt-sync');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks("grunt-browserify");

	grunt.registerTask('default', ['browserify:dev', 'sass:dev', 'sync:dev', 'browserSync', 'watch']);

};