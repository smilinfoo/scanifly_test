# SETUP AND USE #
src/Scanifly_Test.js is a stand in for the main app

## INSTANTIATE AND INITIALIZE ##
```
veg_tool = new Vegetation_Tool();
```
and initializing it
``` 
veg_tool.init(camera, scene, renderer);
```
Camera, scene and renderer are needed for raytracing and to set the needed shadow map override on the renderer

## REQUIRED METHODS ##
### Prep the model ###
```
prep_model(Object3d)
```
Pass in the scene model so the material can be updated to the occludable version.  The texture is copied from the existing material, so the usual material needs to be applied first.

### Update###
```
update()
```
Must be called each render.

## USE ##
### Change mode ###
the modes are:
Vegetation_Tool.MODE.MOVE
Vegetation_Tool.MODE.SIZE
Vegetation_Tool.MODE.ROTATE
Vegetation_Tool.MODE.ADD_CUBE
Vegetation_Tool.MODE.ADD_CYLINDER

```
set_mode(Vegetation_Tool.MODE.MOVE)
```
will switch between modes

### Adding and removing occluders ###
Occluders can be added and removed by switching to the appropriate mode and letting the tools interaction handel it. 
They can also be added by calling 
```
add_occluder_at( point, shape )
```
and
```
remove_occluder( guide )
```
point is a Vector3 in world space.
shape is a constant either Vegetation_Tool.SHAPE.CUBE or Vegetation_Tool.SHAPE.CYLINDER.
guide is the Object3D in the scene representing the occluder.

### Make occlusion active/inactive ###
```
set_active(bool)
```

### Show or hide occlusion guides ###
```
occluders_visible(bool)
```
### Show or hide occlusion guides ###
```
occluders_visible(bool)
```

### Change camera (when moving to an orthological view) ###
```
update_camera(new_camera)
```


# TEST JIG INSRUCTIONS #
## occluders_visible ##
Toggles occluder guids on and off.

## occluders_active ##
Are they ... occluding

## mode ###
Switches between ADD, REMOVE, MOVE and SIZE

### ADD ###
Mouse click adds a new new occluder where mouse first interestes a mesh

### REMOVE ###
Clicking an occluder removes it

### MOVE ###
Occluders are dragable

### SIZE ###
Occluder size can be changed by dragging sides.



# DEV #
## Set up #
requires
node v12
npm v6
sass

## Instructions ##
```
cd scanifly_test
npm install
```
dev build and launch
```
npm start
```
build not needed


# NOTES #
While the the vegitation tool code has been seperated form the three.js build, it does still rely on in THREE is importend into the needed file.  If a build system that does not support import is used, we will need to alter the build preocess to export a finge module or global object.

The Transform_Controls from the Three.js examples library is used.  It is included in a subfolder within src/veg_tool to avoid different versions ot the Transform_Controls being a problem.

Shaders are in their own folder src/shaders. While these are required by te vegitation tool, they have been put in a seperate folder so they can be keept with all the projects shader, as mentioned on the phone.

