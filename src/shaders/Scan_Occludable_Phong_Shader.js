//import { UniformsUtils } from 'three'
//import { UniformsLib } from './../js/three.js/src/renderers/shaders/UniformsLib.js'
import { UniformsLib } from 'three'
import { Color } from 'three';

export default {

  uniforms: [
			UniformsLib.common,
			UniformsLib.specularmap,
			UniformsLib.envmap,
			UniformsLib.aomap,
			UniformsLib.lightmap,
			UniformsLib.emissivemap,
			UniformsLib.bumpmap,
			UniformsLib.normalmap,
			UniformsLib.displacementmap,
			UniformsLib.fog,
			UniformsLib.lights,
			{
				emissive: { value: new Color( 0x000000 ) },
				specular: { value: new Color( 0x111111 ) },
				shininess: { value: 30 }
			},
			{
				occ_pos: {value: 0.0},
				occ_active: {value: true},
				occluder_count: {value: 1},
				bounds_array: {value:  new Array().fill(0, 0, 47)},
				occluder_transforms: {value:  new Array().fill(0, 0, 127) },
				occluder_cy_count: {value: 1},
				bounds_cy_array: {value:  new Array().fill(0, 0, 47)},
				occluder_cy_transforms: {value:  new Array().fill(0, 0, 127) },
			}
  ],

  vertexShader: `
#define PHONG

varying vec3 vViewPosition;

#ifndef FLAT_SHADED

	varying vec3 vNormal;

#endif

#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <displacementmap_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>

/*SCAN OCC SPECIFIC*/
uniform bool occ_active;
uniform int occluder_count;
uniform float bounds_array[48];
uniform mat4 occluder_transforms[8];
uniform int occluder_cy_count;
uniform float bounds_cy_array[48];
uniform mat4 occluder_cy_transforms[8];
varying float scan_occ;
varying vec3 inter_pos;
uniform sampler2D map;

void main() {

	#include <uv_vertex>
	#include <uv2_vertex>
	#include <color_vertex>

	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>

#ifndef FLAT_SHADED // Normal computed with derivatives when FLAT_SHADED

	vNormal = normalize( transformedNormal );

#endif

	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>

	vViewPosition = - mvPosition.xyz;

	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>

	//Scanifly additions
	scan_occ = 1.0;
	if(occ_active){
		//JCG - this should be optimized for production use.
		//8 is max number of occluders supported
		for(int i = 0; i < 8; i++){
			if(i < occluder_count){
				int ind = i * 6; //6 boundries per set
				vec4 testPos = occluder_transforms[i] * vec4(position,1.0);
				if(testPos.x > bounds_array[ind+0] && testPos.x < bounds_array[ind+1]){
					if(testPos.y > bounds_array[ind+2] && testPos.y < bounds_array[ind+3]){
						if(testPos.z > bounds_array[ind+4] && testPos.z < bounds_array[ind+5]){
							scan_occ = 0.0;
						}
					}
				}
			}
			else {
				break;
			}
			
		}
	}
	inter_pos = vec3(position);

}

  `,

  fragmentShader: `
#define PHONG

uniform vec3 diffuse;
uniform vec3 emissive;
uniform vec3 specular;
uniform float shininess;
uniform float opacity;

#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <uv2_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_pars_fragment>
#include <cube_uv_reflection_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <lights_phong_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>

/*SCAN OCC SPECIFIC*/
uniform bool occ_active;
uniform int occluder_count;
uniform float bounds_array[48];
uniform mat4 occluder_transforms[8];
uniform int occluder_cy_count;
uniform float bounds_cy_array[48];
uniform mat4 occluder_cy_transforms[8];
varying float scan_occ;
//uniform sampler2D map;
//varying vec4 position;
varying vec3 inter_pos;

void main() {

	#include <clipping_planes_fragment>

	vec4 diffuseColor = vec4( diffuse, opacity );
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;

	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <specularmap_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <emissivemap_fragment>

	// accumulation
	#include <lights_phong_fragment>

	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>

	// modulation
	#include <aomap_fragment>

	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;

	#include <envmap_fragment>

	vec4 outColor = vec4( outgoingLight, diffuseColor.a );



	//OCC TEST
	float occ_alpha = 1.0;
	float mult = 0.01;
	vec4 outC = vec4(vViewPosition.x * mult, vViewPosition.y * mult, vViewPosition.z * mult, 1.0);
	float occ_val = 1.0;
	if(occ_active){
		for(int i = 0; i < 8; i++){
			if(i < occluder_count){
				int ind = i * 6; //6 boundries per set
				vec4 testPos = occluder_transforms[i] * vec4(inter_pos,1.0);
				//outC *= 50.0;
				if(testPos.x > bounds_array[i*6] && testPos.x < bounds_array[(i*6)+1]){		
					
					if(testPos.y > bounds_array[(i*6)+2] && testPos.y < bounds_array[(i*6)+3]){

						if(testPos.z > bounds_array[(i*6)+4] && testPos.z < bounds_array[(i*6)+5]){
							occ_alpha = 0.0;	
						}
					}
				}
				
			}
			else
			{
				break;
			}	
		}

		//cylinders
		for(int i = 0; i < 8; i++){
			if(i < occluder_cy_count){
				int ind = i * 6; //6 boundries per set
				vec4 testPos = occluder_cy_transforms[i] * vec4(inter_pos,1.0);
				if(testPos.y > bounds_cy_array[i*6] && testPos.y < bounds_cy_array[(i*6)+1]){	
					//within radius
					float testPos_dist = distance(testPos.xyz, vec3(0.0, testPos.y, 0.0));
					if(testPos_dist < bounds_cy_array[(i*6)+2])
					{
						occ_alpha = 0.0;	
					}	
				}
				
			}
			else
			{
				break;
			}	

		}
	}

	outColor.a = min(outColor.a, occ_alpha);
	gl_FragColor = outColor;

	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>


}
  `
};