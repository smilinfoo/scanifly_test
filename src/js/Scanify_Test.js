//////////////////////////////////////////////////////////////////////////////////
//
// MANAGE SCENE AND CONTROLS
//
//////////////////////////////////////////////////////////////////////////////////
//pulling in Three.js as part of the project source so hijack the WebGLShadowMapClass and Depth Shader
import * as THREE from 'three';
import { OrbitControls } from './three_ext/OrbitControls.js'
import { OBJLoader2 } from './three_ext/OBJLoader2';
import { OBJLoader } from './three_ext/OBJLoader';
import * as dat from 'dat.gui';

import Occluder_Drag_Control from './veg_tool/Occluder_Drag_Control';
import Occlusion_Manager from './veg_tool/Occlusion_Manager';

import Vegetation_Tool from './veg_tool/Vegitation_Tool';

let camera, scene, renderer, controls;
let ortho_camera, active_camera;
let scene_material, current_model, sunLight;
let occluder_controls, rotation_controls;
let veg_tool = new Vegetation_Tool();

let models = {
	original:{
		model:'models/3dmodel/3dmodel_blend.obj',
		texture:"models/3dmodel/3dmodel.jpg",
		scale:1,
		position:{x:0,y:0,z:0},
		rotation:{x:0,y:0,z:0}
	},
	small_commerical:{
		model:'models/small_commercial/3dmodel_alt.obj',
		texture:"models/small_commercial/3dmodel.jpg",
		scale:1,
		position:{x:0,y:0,z:0},
		rotation:{x:0,y:0,z:0}
	},
	with_tree:{
		model:'models/with_tree/3dmodel_alt.obj',
		texture:"models/with_tree/3dmodel.jpg",
		scale:1,
		position:{x:0,y:0,z:0},
		rotation:{x:0,y:0,z:0}
	},
	without_tree:{
		model:'models/without_tree/3dmodel_alt.obj',
		texture:"models/without_tree/3dmodelTex1.jpg",
		scale:1,
		position:{x:0,y:0,z:0},
		rotation:{x:0,y:0,z:0}
	}
}

let gui_options = {
	occluders_visible:true,
	occluder_mode:'move',
	occluders_active:true,
	model:'original',
	azimuth:0,
	altitude:0,
	ortho_view:false
}

const gui = new dat.GUI();
let occ_visible_control = gui.add(gui_options, 'occluders_visible');
let occ_active_control = gui.add(gui_options, 'occluders_active');
let occ_mode_control = gui.add(gui_options, 'occluder_mode', {move:Vegetation_Tool.MODE.MOVE, size:Vegetation_Tool.MODE.SIZE, rotate:Vegetation_Tool.MODE.ROTATE, add_cube:Vegetation_Tool.MODE.ADD_CUBE, add_cylinder:Vegetation_Tool.MODE.ADD_CYLINDER, remove:Vegetation_Tool.MODE.REMOVE});
let occ_model_control = gui.add(gui_options, 'model', ['original', 'small_commerical', 'with_tree',  'without_tree']);
let occ_azimuth = gui.add(gui_options, 'azimuth', 0, 360);
let occ_altitude = gui.add(gui_options, 'altitude', -10, 90);
let occ_ortho = gui.add(gui_options, 'ortho_view');

let occlusion_manager, occ_shadowmap_material;

let sunlight_radius = 50;
let original_azimuth = 0;
let original_altitude = 0;

export default class Scanify_Test
{

	constructor(){
		//INIT CONTROLS AND VEG_TOOL
		this.is_ortho = gui_options.ortho_view;
	}

	// Set up scene and controlls
	init(container){

		//SCENE AND RENDERER SETUP
		scene = new THREE.Scene();
		scene.background = new THREE.Color( 0xADD8E6 );
		//cameras
		camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 10000 );
		camera.position.y = 20;
		camera.position.z = 30;
		//setup ortho camera
		let aspect = window.innerHeight / window.innerWidth ;
		let ortho_size = 75;
		ortho_camera = new THREE.OrthographicCamera( ortho_size/-2, ortho_size/2,  (ortho_size*aspect) / 2,  (ortho_size*aspect) / - 2, 1, 1000 );
		ortho_camera.position.x = 0;
		ortho_camera.position.y = 50;
		ortho_camera.position.z = 0;
		ortho_camera.lookAt(0,0,0);
		scene.add(ortho_camera);
		//set perspective as active camera
		active_camera = camera;	

		//renderer
		renderer = new THREE.WebGLRenderer( { antialias: true, preserveDrawingBuffer: true } );
		renderer.setPixelRatio( window.devicePixelRatio );
		renderer.setSize( window.innerWidth, window.innerHeight );
		container.appendChild( renderer.domElement );

		//lights
		var ambient = new THREE.AmbientLight(0x404040);
		scene.add(ambient);
		sunLight = new THREE.DirectionalLight( 0xffffff, 0.5  );
		sunLight.castShadow = true;
		sunLight.shadow.bias = -0.002;
		sunLight.shadow.mapSize.width  = 4096;
		sunLight.shadow.mapSize.height = 4096;
		this.set_sun_pos(sunLight, sunlight_radius, original_azimuth, original_altitude);
		scene.add(sunLight);34



		//INIT THE VEG_TOOL
		veg_tool = new Vegetation_Tool();
		veg_tool.init(camera, scene, renderer);



		//LOAD MODEL
		let inital_model = models.original;
		this.load_model(inital_model.model, inital_model.texture, inital_model.scale, inital_model.position, inital_model.rotation);


		//CONTROLS
		//camera
		controls = new OrbitControls( active_camera, renderer.domElement );
		controls.enablePan = true;


		//gui controls
		occ_mode_control.onChange((val)=>{
			//occluder_controls.mode = val;
			veg_tool.set_mode(val);
		})

		occ_visible_control.onChange((val)=>{
			if(val){
				veg_tool.occluders_visible(val);
			}
			else
			{
				veg_tool.occluders_visible(val);
				//asdasd
			}
		});

		occ_active_control.onChange((val)=>{
			veg_tool.set_active(val);
		});

		occ_model_control.onChange((val)=>{
			this.load_model(models[val].model, models[val].texture, models[val].scale, models[val].position, models[val].rotation);
		})

		occ_azimuth.onChange((val)=>{
			this.set_sun_pos(sunLight, sunlight_radius, gui_options.azimuth, gui_options.altitude);
		});
		
		occ_altitude.onChange((val)=>{
			this.set_sun_pos(sunLight, sunlight_radius, gui_options.azimuth, gui_options.altitude);
		});	

		occ_ortho.onChange((val)=>{
			this.set_ortho(val);
		});	

		//add first occluder
		//var occ_display = this.add_occluder_cy(new THREE.Vector3(0,6,6)); //0 10 8
		//var occ_display = this.add_occluder(new THREE.Vector3(0,6,6)); //0 10 8
		this.add_occluder(new THREE.Vector3(-20,12,0));// - plops occluder down in tree on left of original test model
		//this.add_occluder_cy(new THREE.Vector3(0,6,6));
	}


	load_model(model_url, texture_url, scale, pos, rot){
		//let scene_material = occlusion_manager.get_occludable_material(   new THREE.TextureLoader().load( texture_url )   );
		//scene_material = occlusion_manager.get_occludable_shadow_material();

		if(current_model){
			scene.remove(current_model);
		}

		let texture =  new THREE.TextureLoader().load( texture_url );
		let mat = new  THREE.MeshPhongMaterial({map:texture, side:THREE.DoubleSide, shadowSide:THREE.DoubleSide});

		//load model
		let loader = new OBJLoader2();
		let loaded = (obj3d)=>{
			obj3d.children[0].material = mat;
			obj3d.scale.set(scale,scale, scale);
			obj3d.rotation.set(rot.x, rot.y, rot.z);
			obj3d.position.set(pos.x,pos.y,pos.z);
			obj3d.children[0].castShadow = true; //default is false
			obj3d.children[0].receiveShadow = true; //default
			current_model = obj3d;
			scene.add( obj3d );

			//update light to fix box
			var bbox = new THREE.Box3();
			bbox.expandByObject(obj3d);
			var l = bbox.max.clone().sub(bbox.min).length();
			sunLight.shadow.camera.near   =  10;
			sunLight.shadow.camera.far    =  l * 1.5;
			sunLight.shadow.camera.right  =  l/2;
			sunLight.shadow.camera.left   = -l/2;
			sunLight.shadow.camera.top    =  l/2;
			sunLight.shadow.camera.bottom = -l/2;
			sunLight.shadow.camera.updateProjectionMatrix();

			//var helper = new THREE.CameraHelper( sunLight.shadow.camera );
			//scene.add( helper );
			veg_tool.prep_model(obj3d);
		};
		loader.setModelName( "property" );
		loader.setLogging( true, true );
		loader.load( model_url, loaded, null, null, null );
	}


	load_obj(model_url, texture_url){
		var loader = new OBJLoader();
		loader.load(
			model_url,
			function ( object ) {//success
				scene.add( object );
			},
			function ( xhr ) { // progress
				console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
			},
			function ( error ) {// error
				console.log( 'Coule Not load model:'+model_url+":"+texture_url);
			}
		);
	}


	//add an occluder
	add_occluder(point){
		let occluder =  veg_tool.add_occluder_at(new THREE.Vector3(0,6,6), 'CUBE');
	}

	//add a cylinder occluder
	add_occluder_cy(point){
		let occluder =  veg_tool.add_occluder_at(new THREE.Vector3(0,6,6), 'CYLINDER');
	}

	//remove an occluder
	remove_occluder(occluder_display){
		scene.remove(occluder_display);
		veg_tool.remove_occluder(occluder_display);
	}

	set_sun_pos(obj, radius, azimuth, altitude){

		let sun_vector = new THREE.Vector3(0, 0, -radius);
		let heading_radians = (0-azimuth) * Math.PI/180; //-90 to make z axis 0
		let alt_radians = altitude*Math.PI/180;
		let euler = new THREE.Euler(alt_radians, heading_radians, 0, 'YXZ' );
		sun_vector.applyEuler(euler);
		
		let alt_radian = altitude * Math.PI/180;
		let x = sun_vector.x;
		let z = sun_vector.z
		let y = sun_vector.y;

		obj.position.set(x, y, z);
		
	}


	//start update loop
	start(){

		this.update();

	}

	//update loop
	update(){

		requestAnimationFrame( ()=>{ this.update() } );
		veg_tool.update();
		renderer.render( scene, active_camera );

	}

	go_ortho(){
		//record camera position and heading
		//or just switch the rendering camera
		//lock occluder controls and show trident
		ortho_camera.position.x = 0;
		ortho_camera.position.y = 50;
		ortho_camera.position.z = 0;
		ortho_camera.lookAt(0,0,0);
		active_camera = ortho_camera;
		controls.update_camera(active_camera);
		veg_tool.update_camera(active_camera);
		this.is_ortho = true;
	}

	go_normal(){
		//return to normal camera
		//remove trident and return to original occluder controls
		active_camera = camera;	
		controls.update_camera(active_camera);
		veg_tool.update_camera(active_camera);
		this.is_ortho = false;
	}

	set_ortho(is_ortho){
		if(is_ortho != this.is_ortho){
			if(is_ortho){
				this.go_ortho();
			}
			else
			{
				this.go_normal();
			}
		}

	}
}