import * as THREE from 'three';
import occ_shader from './../../shaders/Scan_Occludable_Phong_Shader';


//test values - real values should pull from source outside of threejs
//import * as SL from './three.js/src/renderers/shaders/ShaderLib'
import { UniformsUtils } from 'three';


export default class Occludable_Material extends THREE.MeshPhongMaterial{

	constructor(config){
		/*
		super({
				...occ_shader,
				fog: true,
				lights: true,
				dithering: true,
				transparent:true,
			}
		);
		*/
		config.transparent = true;
		config.shininess = 1;
		config.reflectivity = 1;
		config.specular = 0x000000;
		super(config);
		this.type = "none";
		this.uniforms = UniformsUtils.merge(occ_shader.uniforms);
		this.vertexShader = occ_shader.vertexShader;
		this.fragmentShader = occ_shader.fragmentShader;
	}

}