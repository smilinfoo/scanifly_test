//////////////////////////////////////////////////////////////////////////////////
//
//	Track occluded areas
//	generate texture for model to be occluded
//	generate occluder area guides
//	update uniforms to hide faces
//
//////////////////////////////////////////////////////////////////////////////////
import * as THREE from 'three';
import occ_mat from './Occludable_Material';
import occ_shadow_mat from './Occludable_Shadow_Material';

//occluider defaults
const default_size = 1;
const max_occluders = 8;


//material to be used on model
let occludable_material;
let occludable_shadow;

//material used on occlusion area guides
let occluder_guide_material = new THREE.MeshNormalMaterial({
	side: THREE.DoubleSide,
	transparent:true,
	opacity:0.2
});

//material used on occlusion area guides
let occluder_guide_highlight_material = new THREE.MeshNormalMaterial({
	side: THREE.DoubleSide,
	transparent:true,
	opacity:0.6
});

let initial_mat = true; //!!!!!!!!!!!!!!!!!!!!!! REMOVE THIS

export default class Occlusion_Manager{

	
	constructor(){
		this.occluders = [];
		this.occluders_cy = [];
		this.unis =  {
			pos:0.0,
			count:1,
			bounds:new Array().fill(0, 0, 47),
			transforms:new Array().fill(0, 0, 127),
			active:true
		}
	}


	get_occludable_material( texture ){
	
		occludable_material = new occ_mat({map:texture, side:THREE.DoubleSide, shadowSide:THREE.DoubleSide});
		return occludable_material;

	}

	get_occludable_shadow_material(){

		if(!occludable_shadow){
			occludable_shadow = new occ_shadow_mat();
		}
		return occludable_shadow;
		
	}


	get_occluder_at(x,y,z){

		if(this.occluders.length >= max_occluders)
		{
			return false;
		}
		else
		{
			let occ = new Occluder(x,y,z);
			this.occluders.push(occ);
			return occ.get_display();			
		}

	}

	get_occluder_cy_at(x,y,z){

		if(this.occluders.length >= max_occluders)
		{
			return false;
		}
		else
		{
			let occ = new Occluder_CY(x,y,z);
			this.occluders_cy.push(occ);
			return occ.get_display();			
		}

	}



	remove_by_display(occluder_display){

		for(let i = 0 ; i < this.occluders.length ; i++)
		{
			if(occluder_display ===   this.occluders[i].occ_container)
			{
				this.occluders.splice(i,1);
				break;				
			}
		}

		for(let i = 0 ; i < this.occluders_cy.length ; i++)
		{
			if(occluder_display ===   this.occluders_cy[i].occ_container)
			{
				this.occluders_cy.splice(i,1);
				break;				
			}
		}

	}


	hover_on(obj, parent){
		if(parent){
			parent.traverse(function(ob){
				ob.material = occluder_guide_material;
			});
		}

		obj.traverse(function(ob){
			ob.material = occluder_guide_highlight_material;
		});				
	}


	hover_off(obj){
		obj.traverse(function(ob){
			ob.material = occluder_guide_material;
		});			
	}

	//update area and pass to shader
	update(){

		let bounds = [];
		let transforms = [];

		let bounds_cy = [];
		let transforms_cy = [];

		//get bounds and transform
		for(let i = 0 ; i < this.occluders.length ; i++)
		{
			this.occluders[i].update();
			bounds = bounds.concat( this.occluders[i].bounds );
			transforms = transforms.concat(  this.occluders[i].inv_transform.toArray()  );
		}

		if(bounds.length == 0)
		{
			bounds = new Array(48).fill(0);
			transforms = new Array(128).fill(0);
		}

		for(let i = 0 ; i < this.occluders_cy.length ; i++)
		{
			this.occluders_cy[i].update();
			bounds_cy = bounds_cy.concat( this.occluders_cy[i].bounds );
			transforms_cy = transforms_cy.concat(  this.occluders_cy[i].inv_transform.toArray()  );
		}

		if(bounds_cy.length == 0)
		{
			bounds_cy = new Array(48).fill(0);
			transforms_cy = new Array(128).fill(0);
		}

		//bounds and transforms passed as array of floats
		
		if(occludable_material)
		{
			occludable_material.uniforms[ 'occluder_count' ].value = this.occluders.length;
			occludable_material.uniforms[ 'bounds_array' ].value = bounds;
			occludable_material.uniforms[ 'occluder_transforms' ].value = transforms;

			occludable_material.uniforms[ 'occluder_cy_count' ].value = this.occluders_cy.length;
			occludable_material.uniforms[ 'bounds_cy_array' ].value = bounds_cy;
			occludable_material.uniforms[ 'occluder_cy_transforms' ].value = transforms_cy;				
		}


		//occludable_shadow	
		if(occludable_shadow){
			occludable_shadow.uniforms[ 'occluder_count' ].value = this.occluders.length;
			occludable_shadow.uniforms[ 'bounds_array' ].value = bounds;
			occludable_shadow.uniforms[ 'occluder_transforms' ].value = transforms;

			occludable_shadow.uniforms[ 'occluder_cy_count' ].value = this.occluders_cy.length;
			occludable_shadow.uniforms[ 'bounds_cy_array' ].value = bounds_cy;
			occludable_shadow.uniforms[ 'occluder_cy_transforms' ].value = transforms_cy;				
		}
		

		//set data that will be sent to shadowmap
		this.unis.pos = -20;
		this.unis.count = this.occluders.length;
		this.unis.bounds = bounds;
		this.unis.transforms = transforms;
		this.unis.count_cy = this.occluders_cy.length;
		this.unis.bounds_cy = bounds_cy;
		this.unis.transforms_cy = transforms_cy;
	}


	//return the uniform data to pass to shadowmap
	get_unis(){
		return this.unis;
	}


	//show/hid guide planes
	set show_guides(val){
		for(let i = 0 ; i < this.occluders.length ; i++)
		{
			this.occluders[i].occ_container.traverse((child)=>{
				child.visible = val;
			});
		}	

		for(let i = 0 ; i < this.occluders_cy.length ; i++)
		{
			this.occluders_cy[i].occ_container.traverse((child)=>{
				child.visible = val;
			});	
		}	

	}


	//tuen on and off occlusion
	set occluder_active(val){
		occludable_material.uniforms[ 'occ_active' ].value = val;
		occludable_shadow.uniforms[ 'occ_active' ].value = val;
		this.unis.active = val;
	}
}


//////////////////////////////////////////////////////
//
//	Individual area to hide
//	keep private to Occluder manager
//	
////////////////////////////////////////////////////////
class Occluder{


	constructor(x,y,z){
		this.x = x;
		this.y = y
		this.z = z;

		this._inv_transform = new THREE.Matrix4();
	}


	//return a display object to represent the occlusion area
	get_display(){

		let rots = [
			new THREE.Vector3(0,1,0),  //top 0
			new THREE.Vector3(0,-1,0), //bottom 1
			new THREE.Vector3(0,0,1), //front 2
			new THREE.Vector3(0,0,-1), //back 3
			new THREE.Vector3(-20,0,0), //left 4
			new THREE.Vector3(1,0,0) //right 5
		];

		let init_dist = 1;
		let plane_geo = new THREE.PlaneGeometry( init_dist*2, init_dist*2);

		this.occ_planes = [];
		this.occ_container = new THREE.Object3D();

		for(let i = 0 ; i < rots.length ;i++){

			let p_mesh = new THREE.Mesh(plane_geo, occluder_guide_material);
			p_mesh.lookAt(rots[i]);
			let dir = new THREE.Vector3();
			p_mesh.getWorldDirection(dir);
			p_mesh.position.addScaledVector(dir, init_dist);
			p_mesh.castShadow = false;
			p_mesh.receiveShadow = false;
			this.occ_planes.push(p_mesh);

			this.occ_container.add( p_mesh);
		}

		this.occ_container.position.set(this.x,this.y,this.z);	
		this.occ_container['type'] = 'box';

		return this.occ_container;	
	}

	highlight(){
		this.occ_container.traverse(function(ob){
			ob.material = occluder_guide_highlight_material;
		});
	}


	update(){

		//find dimensions 
		let z_span = (this.occ_planes[2].position.z - this.occ_planes[3].position.z)*0.5;
		let z_center = this.occ_planes[3].position.z + (z_span);
		let x_span = (this.occ_planes[5].position.x - this.occ_planes[4].position.x)*0.5;
		let x_center = this.occ_planes[4].position.x + (x_span);
		let y_span = (this.occ_planes[0].position.y - this.occ_planes[1].position.y)*0.5;
		let y_center = this.occ_planes[1].position.y + (y_span);		

		//size guid plane 
		this.occ_planes[4].scale.x = this.occ_planes[5].scale.x = this.occ_planes[0].scale.y = this.occ_planes[1].scale.y = z_span;
		this.occ_planes[2].position.z = z_span;
		this.occ_planes[3].position.z = -z_span;
		//this.occ_planes[2].position.y = this.occ_planes[3].position.y = z_span;
		//this.occ_planes[3].position.x = -z_span * 0.5;
		//this.occ_planes[4].position.z = this.occ_planes[5].position.z = this.occ_planes[0].position.z = this.occ_planes[1].position.z = z_center;
		this.occ_planes[2].scale.x = this.occ_planes[3].scale.x = this.occ_planes[0].scale.x = this.occ_planes[1].scale.x = x_span;
		this.occ_planes[4].position.x =  -x_span;
		this.occ_planes[5].position.x = x_span;
		//this.occ_planes[2].position.x = this.occ_planes[3].position.x = this.occ_planes[0].position.x = this.occ_planes[1].position.x = x_center;
		this.occ_planes[4].scale.y = this.occ_planes[5].scale.y = this.occ_planes[2].scale.y = this.occ_planes[3].scale.y = y_span;
		this.occ_planes[0].position.y = y_span;
		this.occ_planes[1].position.y = -y_span;
		//this.occ_planes[0].position.z = this.occ_planes[1].position.z = y_span * 0.5;
		//this.occ_planes[4].position.y = this.occ_planes[5].position.y = this.occ_planes[2].position.y = this.occ_planes[3].position.y = y_center;
	}


	get bounds(){

		return [
			this.occ_planes[4].position.x,
			this.occ_planes[5].position.x,
			this.occ_planes[1].position.y,
			this.occ_planes[0].position.y,
			this.occ_planes[3].position.z,
			this.occ_planes[2].position.z,
		]

	}


	get inv_transform(){

		this._inv_transform.getInverse( this.occ_container.matrixWorld );
		return this._inv_transform;

	}
}




class Occluder_CY{


	constructor(x,y,z){
		this.x = x;
		this.y = y
		this.z = z;

		this.mesh;

		this._inv_transform = new THREE.Matrix4();
	}


	//return a display object to represent the occlusion area
	get_display(){


		let init_dist = 1;
		let plane_geo = new THREE.CylinderGeometry( init_dist, init_dist, init_dist, 32);
		let cy_mesh = new THREE.Mesh(plane_geo, occluder_guide_material);
		//let dir = new THREE.Vector3();
		//cy_mesh.getWorldDirection(dir);
		//cy_mesh.position.addScaledVector(dir, init_dist);
		cy_mesh.castShadow = false;
		cy_mesh.receiveShadow = false;
		this.mesh = cy_mesh;


		this.occ_container = new THREE.Object3D();
		this.occ_container.add( cy_mesh );

		this.occ_container.position.set(this.x,this.y,this.z);
		this.occ_container['type'] = 'cylinder';

		return this.occ_container;	

	}


	update(){

		//find dimensions 
		/*
		let z_span = (this.occ_planes[2].position.z - this.occ_planes[3].position.z)*0.5;
		let z_center = this.occ_planes[3].position.z + (z_span);
		let x_span = (this.occ_planes[5].position.x - this.occ_planes[4].position.x)*0.5;
		let x_center = this.occ_planes[4].position.x + (x_span);
		let y_span = (this.occ_planes[0].position.y - this.occ_planes[1].position.y)*0.5;
		let y_center = this.occ_planes[1].position.y + (y_span);		


		//size guid plane 
		this.occ_planes[4].scale.x = this.occ_planes[5].scale.x = this.occ_planes[0].scale.y = this.occ_planes[1].scale.y = z_span;
		this.occ_planes[2].position.z = z_span;
		this.occ_planes[3].position.z = -z_span;
		//this.occ_planes[2].position.y = this.occ_planes[3].position.y = z_span;
		//this.occ_planes[3].position.x = -z_span * 0.5;
		//this.occ_planes[4].position.z = this.occ_planes[5].position.z = this.occ_planes[0].position.z = this.occ_planes[1].position.z = z_center;
		this.occ_planes[2].scale.x = this.occ_planes[3].scale.x = this.occ_planes[0].scale.x = this.occ_planes[1].scale.x = x_span;
		this.occ_planes[4].position.x =  -x_span;
		this.occ_planes[5].position.x = x_span;
		//this.occ_planes[2].position.x = this.occ_planes[3].position.x = this.occ_planes[0].position.x = this.occ_planes[1].position.x = x_center;
		this.occ_planes[4].scale.y = this.occ_planes[5].scale.y = this.occ_planes[2].scale.y = this.occ_planes[3].scale.y = y_span;
		this.occ_planes[0].position.y = y_span;
		this.occ_planes[1].position.y = -y_span;
		//this.occ_planes[0].position.z = this.occ_planes[1].position.z = y_span * 0.5;
		//this.occ_planes[4].position.y = this.occ_planes[5].position.y = this.occ_planes[2].position.y = this.occ_planes[3].position.y = y_center;
		*/
	}


	get bounds(){

		/*
		return [
			this.occ_planes[4].position.x,
			this.occ_planes[5].position.x,
			this.occ_planes[1].position.y,
			this.occ_planes[0].position.y,
			this.occ_planes[3].position.z,
			this.occ_planes[2].position.z,
		]
		*/
		let y_scale_half = this.mesh.scale.y * 0.5;
		let radius_scale = this.mesh.scale.x;
		return [ -y_scale_half, y_scale_half, radius_scale, 0, 0, 0]
		//return [ -1, 1, 1, 0, 0, 0]

	}


	get inv_transform(){

		this._inv_transform.getInverse( this.occ_container.matrixWorld );
		return this._inv_transform;

	}
}