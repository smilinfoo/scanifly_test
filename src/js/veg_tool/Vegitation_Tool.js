import * as THREE from 'three';
import Occluder_Drag_Control from './Occluder_Drag_Control';
import Occlusion_Manager from './Occlusion_Manager';


let occluder_controls, occlusion_manager;
let _camera, _scene, _renderer;

export default class Vegetation_Tool{

	constructor(){
		
	}

	init( camera, scene, renderer ){
		_camera = camera;
		_scene = scene;
		_renderer = renderer;

		_renderer.shadowMap.enabled = true;
		_renderer.shadowMap.type = THREE.PCFSoftShadowMap; 
		_renderer.outputEncoding = THREE.sRGBEncoding;

		occlusion_manager = new Occlusion_Manager();
		occluder_controls =  new Occluder_Drag_Control( [], camera, scene, renderer.domElement);
		occluder_controls.set_mode('move');

		occluder_controls.addEventListener( 'add_occluder', ( event )=> {
			this.add_occluder_at(event.object, 'CUBE');
		} );

		occluder_controls.addEventListener( 'add_occluder_cy', ( event )=> {
			this.add_occluder_at(event.object, 'CYLINDER');
		} );

		occluder_controls.addEventListener( 'remove_occluder', ( event )=> {
			this.remove_occluder(event.object);
		} );

		occluder_controls.addEventListener( 'add_rotator', ( event )=> {
			scene.add( event.object );
		} );

		occluder_controls.addEventListener( 'rotatestart', ( event )=> {
			scene.add(event.hud);
		} );

		occluder_controls.addEventListener( 'rotateend', ( event )=> {
			//stub
		} );

		occluder_controls.addEventListener( 'hoveron', ( event )=> {
			occlusion_manager.hover_on(event.object, event.parent);
		} );

		occluder_controls.addEventListener( 'hoveroff', ( event )=> {
			occlusion_manager.hover_off(event.object);
		} );

		occluder_controls.addEventListener( 'hoveron_sub', ( event )=> {
			//console.log(event.object);
		} );

		occluder_controls.addEventListener( 'hoveroff_sub', ( event )=> {
			//console.log(event.object);
		} );
	}
	

	// model:Object3D 
	//return void
	prep_model( model ){
		//Model needs to have material and texture applied already
		//Veg tool will use the texture form the applied material to create a new material with the same texture and apply it
		//Veg tool sets override shadow depth material on object
		let temp_mat, occ_mat;
		model.traverse(function(node){
			if(node.material && node.material.map && !occ_mat){
				occ_mat = occlusion_manager.get_occludable_material(  node.material.map   );
				node.material = occ_mat;
				node.customDepthMaterial = occlusion_manager.get_occludable_shadow_material();
			}
		});

	}

	// point:vector3, shape:string
	//return Object3D
	add_occluder_at( point, shape ){
		//Returns a Object3D that matches occluded area
		//Added to scene by app
		let occluder_guide;

		if(shape == 'CYLINDER')
		{
			occluder_guide = occlusion_manager.get_occluder_cy_at(point.x, point.y, point.z);
			
		}
		else
		{
			occluder_guide = occlusion_manager.get_occluder_at(point.x, point.y, point.z);
		}

		if(occluder_guide){
			occluder_guide.castShadow = false;
			occluder_guide.receiveShadow = false;
			occluder_controls.addObject(occluder_guide);	
			_scene.add(occluder_guide);		
		}

		return occluder_guide;

	}

	remove_occluder( guide ){
		occlusion_manager.remove_by_display( guide );
		occluder_controls.removeObject( guide );
		_scene.remove( guide );
	}

	//mode:string
	//returns void (or bool  or other confirmation if needed)
	set_mode( mode ){
		//Changes between veg tool control modes (add, remove, scale, rotate)
		console.log("MODE:"+ mode);
		switch(Number(mode)){
			case Vegetation_Tool.MODE.SIZE:
				occluder_controls.set_mode('size');
				break;

			case Vegetation_Tool.MODE.ROTATE:
				occluder_controls.set_mode('rotate');
				break;

			case Vegetation_Tool.MODE.ADD_CYLINDER:
				occluder_controls.set_mode('add_cylinder');
				break;

			case Vegetation_Tool.MODE.ADD_CUBE:
				occluder_controls.set_mode('add');
				break;

			case Vegetation_Tool.MODE.REMOVE:
				occluder_controls.set_mode('remove');
				break;

			case Vegetation_Tool.MODE.MOVE:
			default:
				occluder_controls.set_mode('move');
		}
		//occluder_controls.set_mode(mode);
	}
		
	//val:bool
	//return void
	set_active( val ){
		//Makes occluders active or not
		occlusion_manager.occluder_active = val;

	}

	//val:bool
	//return void
	occluders_visible( val )
	{
		//Makes occluders guides visible or not
		//Returns new state	
		if(val){
			occluder_controls.activate();
		}
		else
		{
			occluder_controls.deactivate();
		}
		occlusion_manager.show_guides = val;	
	}

	//val:bool
	//return void
	occluder_controls_active(val){
		//Turns on or off occluder l
		occlusion_manager.occluder_active = val;	
	}

	update_camera(new_camera){
		occluder_controls.update_camera(new_camera);
	}

	update(){
		occlusion_manager.update();
	}

}

Vegetation_Tool.MODE = {
	MOVE:10,
	SIZE:20,
	ROTATE:30,
	ADD_CUBE:40,
	ADD_CYLINDER:50,
	REMOVE:60
}

Vegetation_Tool.SHAPE = {
	CUBE:110,
	CYLINDER:120
}