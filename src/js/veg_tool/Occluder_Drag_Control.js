/**
 * @author zz85 / https://github.com/zz85
 * @author mrdoob / http://mrdoob.com
 * Running this will allow you to drag three.js objects around the screen.
 * @author jcgray - modified
 */
 ///////////////////////////////////////////////////////////////////////////
 //
 // MODIFIED FROM THREE.DragControls
 //	added listens for mosue interaction and respondes based on 'mode' variable
 //	ES5 class not used as this is simply a modification to an existing tool
 //
 ////////////////////////////////////////////////////////////////////////////
import * as THREE from 'three';
import { TransformControls, TransformControlsGizmo, TransformControlsPlane } from './three_ext_dep/Transform_Controls';

let OccDragControls = function ( _objects, _camera, _scene, _domElement ) {

	var _plane = new THREE.Plane();
	var _raycaster = new THREE.Raycaster();

	var _mouse = new THREE.Vector2();
	var _mouse_down = new THREE.Vector2();
	var _mouse_movement = new THREE.Vector2();
	var _offset = new THREE.Vector3();
	var _planeMag = 0;
	var _intersectionMag = 0;
	var _intersection = new THREE.Vector3();
	var _worldPosition = new THREE.Vector3();
	var _inverseMatrix = new THREE.Matrix4();
	var _mouse_3d = new THREE.Vector3();
	var _selected_normal = new THREE.Vector3();
	var _pos = new THREE.Vector3();
	var _quat = new THREE.Quaternion();
	var _quat_inv = new THREE.Quaternion();
	var _select_point_local = new THREE.Vector3();
	var _cy_scale_x = 1;
	var _cy_scale_y = 1;
	var _cy_size_mode = 'side';
	var _local_select_point = new THREE.Vector3();
	var _camera = _camera;

	var _rotate_control = new TransformControls(_camera, _domElement );
	_rotate_control.setMode('rotate');

	var _selected = null, _hovered = null;

	var mode = 'add';

	var scope = this;

	function update_camera(new_camera){
		_camera = new_camera;
	}

	function activate() {

		_domElement.addEventListener( 'mousemove', onDocumentMouseMove, false );
		_domElement.addEventListener( 'mousedown', onDocumentMouseDown, false );
		_domElement.addEventListener( 'mouseup', onDocumentMouseCancel, false );
		_domElement.addEventListener( 'mouseleave', onDocumentMouseCancel, false );
	}

	function deactivate() {

		_domElement.removeEventListener( 'mousemove', onDocumentMouseMove, false );
		_domElement.removeEventListener( 'mousedown', onDocumentMouseDown, false );
		_domElement.removeEventListener( 'mouseup', onDocumentMouseCancel, false );
		_domElement.removeEventListener( 'mouseleave', onDocumentMouseCancel, false );

	}

	function dispose() {

		deactivate();

	}

	function onDocumentMouseMove( event ) {

		event.preventDefault();

		var rect = _domElement.getBoundingClientRect();

		_mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
		_mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;
		_mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
		_mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;
		_mouse_movement.subVectors(_mouse, _mouse_down);
		_mouse_movement.multiplyScalar(20);

		_raycaster.setFromCamera( _mouse, _camera );

		if ( _selected && scope.enabled ) {

			if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

				let newPos =  _intersection.sub( _offset ).applyMatrix4( _inverseMatrix );
				if(scope.mode == 'size' && _selected.parent.type == 'box'){

					_mouse_3d.set(_mouse_movement.x, _mouse_movement.y, 0);
					_mouse_3d.applyQuaternion (_camera.quaternion );
					_mouse_3d.applyQuaternion ( _quat_inv );
					_selected_normal.set(0,0,1);
					_selected_normal.applyQuaternion(_selected.quaternion);
					let dot_product = _selected_normal.dot(_mouse_3d);
					_selected_normal.multiplyScalar(Math.max(_planeMag + dot_product, 0.05));
					let move_vector = new THREE.Vector3();
					move_vector.subVectors(_selected_normal, _selected.position);
					_selected.position.copy(_selected_normal);
					move_vector.multiplyScalar(0.5);
					let inv = _quat_inv.clone();
					inv.inverse();
					move_vector.applyQuaternion(inv);
					_selected.parent.position.add(move_vector);

				}else if (scope.mode == 'size'  && _selected.parent.type == 'cylinder'){


					if(  _cy_size_mode == "end"){ //size from end
						//bring mouse movment into local space
						_mouse_3d.set(_mouse_movement.x, _mouse_movement.y, 0);
						_mouse_3d.applyQuaternion (_camera.quaternion );
						_mouse_3d.applyQuaternion ( _quat_inv );
						_selected_normal.set(0, _select_point_local.y*2.0 ,0);
						_selected_normal.add(_mouse_3d);
						_selected.scale.y = Math.max(Math.abs(_selected_normal.y), 0.1);
						_selected_normal.multiplyScalar(0.5);
						let move = (_selected.scale.y - _cy_scale_y)*0.5;
						if(_selected_normal.y < 0 ){
							move = 0-move;
						}
						let _move_vect = new THREE.Vector3(0,move,0);
						_move_vect.applyQuaternion(_selected.parent.quaternion);
						_selected.parent.position.addVectors(_pos, _move_vect);
					} else {
						_mouse_3d.set(_mouse_movement.x, _mouse_movement.y, 0);
						_mouse_3d.applyQuaternion (_camera.quaternion );
						_mouse_3d.applyQuaternion ( _quat_inv );
						_mouse_3d.y = 0;
						_selected_normal.set(_select_point_local.x, 0 ,_select_point_local.z);
						_selected_normal.add(_mouse_3d);
						let new_scale = _selected_normal.length();

						_selected.scale.x = Math.max(new_scale, 0.1);
						_selected.scale.z = _selected.scale.x;
						//_selected.scale.y = Math.max(_cy_scale_y + fact_Y, 0.1);						
					}

				}else if (scope.mode == 'rotate'){
					//_selected.position.copy( newPos );
					return;
				}
				else{
					_selected.position.copy( newPos );
				}

			}

			scope.dispatchEvent( { type: 'drag', object: _selected } );

			return;

		}

		_raycaster.setFromCamera( _mouse, _camera );

		var intersects = _raycaster.intersectObjects( _objects, true );

		if ( intersects.length > 0 ) {

			var object = intersects[ 0 ].object;

			_plane.setFromNormalAndCoplanarPoint( _camera.getWorldDirection( _plane.normal ), _worldPosition.setFromMatrixPosition( object.matrixWorld ) );

			if ( _hovered !== object ) {

				var target, parent;
				if(scope.mode == "size"){
					target = object;
					parent = object.parent;
				}
				else
				{
					target = object.parent;
					parent = false;
				}
				scope.dispatchEvent( { type: 'hoveron', object: target, parent: parent} );

				_domElement.style.cursor = 'pointer';
				_hovered = target;

			}

		} else {

			if ( _hovered !== null ) {

				scope.dispatchEvent( { type: 'hoveroff', object: _hovered } );
				_domElement.style.cursor = 'auto';
				_hovered = null;

			}

		}

	}

	function onDocumentMouseDown( event ) {

		event.preventDefault();

		_raycaster.setFromCamera( _mouse, _camera );
		var intersects;

		if(scope.mode == "add"){
			intersects = _raycaster.intersectObjects( _scene.children, true );
			if(intersects.length > 0){
				scope.dispatchEvent( { type: 'add_occluder', object: intersects[ 0 ].point  } );
			}
			return;	
		}
		if(scope.mode == "add_cylinder"){
			intersects = _raycaster.intersectObjects( _scene.children, true );
			if(intersects.length > 0){
				scope.dispatchEvent( { type: 'add_occluder_cy', object: intersects[ 0 ].point  } );
			}
			return;	
		}
		else if(scope.mode == "remove"){
			intersects = _raycaster.intersectObjects( _objects, true );
			if(intersects.length > 0){
				scope.dispatchEvent( { type: 'remove_occluder', object: intersects[ 0 ].object.parent } );
			}
			return;
		}

		intersects = _raycaster.intersectObjects( _objects, true );

		if ( intersects.length > 0 ) {

			_mouse_down.copy(_mouse)

			if(scope.mode == 'size' ){
				_selected = intersects[ 0 ].object;	
				if(_selected.parent.type == 'box' ){
					_planeMag = intersects[ 0 ].object.position.length();
					_pos = _selected.parent.position;
					_quat = _selected.parent.quaternion.clone();
					_quat_inv = _selected.parent.quaternion.clone().inverse();
				} else if(_selected.parent.type == 'cylinder'){
					//_planeMag = intersects[ 0 ].object.position.length();
					_pos = _selected.parent.position.clone();
					_quat = _selected.parent.quaternion.clone();
					_quat_inv = _selected.parent.quaternion.clone().inverse();
					_select_point_local = intersects[ 0 ].point.clone();
					_select_point_local = _selected.worldToLocal(_select_point_local);
					_select_point_local.multiply( _selected.scale );

					
					_planeMag = intersects[ 0 ].point.length();
					_cy_scale_x = _selected.scale.x;
					_cy_scale_y = _selected.scale.y;
					
					let box = new THREE.Box3();
					box.setFromObject( _selected );
					let height = box.max.y - box.min.y;
					let actual_y = _select_point_local.y/_selected.scale.y;

					if(Math.abs(actual_y) >= (0.5)*0.99){
						_cy_size_mode = "end";
					}
					else
					{
						_cy_size_mode = "side";
					}
				}
			}
			else
			{
				_selected = intersects[ 0 ].object.parent;
			}
			

			if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

				_inverseMatrix.getInverse( _selected.parent.matrixWorld );
				_offset.copy( _intersection ).sub( _worldPosition.setFromMatrixPosition( _selected.matrixWorld ) );
				_intersectionMag = _intersection.sub( intersects[ 0 ].object.parent.position ).length();

			}

			_domElement.style.cursor = 'move';

			
			if(scope.mode == 'rotate'){
				_rotate_control.attach(_selected);
				scope.dispatchEvent( { type: 'rotatestart', object: _selected, hud:_rotate_control  } );
			}
			else
			{
				scope.dispatchEvent( { type: 'dragstart', object: _selected } );
			}

		}


	}

	function onDocumentMouseCancel( event ) {

		event.preventDefault();

		if ( _selected ) {

			scope.dispatchEvent( { type: 'dragend', object: _selected, hud:_rotate_control } );

			_selected = null;

		}

		_domElement.style.cursor = _hovered ? 'pointer' : 'auto';

	}

	function addObject( o ){
		_objects.push(o);
	}

	function removeObject( o ){
		let ind = _objects.indexOf(o);
		if(ind > -1)
		{
			_objects.splice(ind,1);
		}
	}

	function set_mode(newMode){
		if(newMode == 'rotate'){
			this.mode = newMode;			
		}
		else
		{
			this.mode = newMode;
			_rotate_control.detach();
		}
		
	}



	activate();

	// API

	this.enabled = true;

	this.activate = activate;
	this.deactivate = deactivate;
	this.dispose = dispose;
	this.addObject = addObject;
	this.removeObject = removeObject;
	this.set_mode = set_mode;
	this.update_camera = update_camera;
};

OccDragControls.prototype = Object.create( THREE.EventDispatcher.prototype );
OccDragControls.prototype.constructor = OccDragControls;
export default OccDragControls;
