import * as THREE from 'three';
import occ_shadow_shader from './../../shaders/Scan_Occludable_ShadowMap_Shader';


//test values - real values should pull from source outside of threejs
//import * as SL from './three.js/src/renderers/shaders/ShaderLib'
import { UniformsUtils } from 'three';


export default class Occludable_Shadow_Material extends THREE.ShaderMaterial{

	constructor(config){
		super({
				uniforms: occ_shadow_shader.uniforms,
				vertexShader: occ_shadow_shader.vertexShader,
				fragmentShader: occ_shadow_shader.fragmentShader
			}
		);
		//console.log("SHADOW LOADEDD-------------------------------------");
	}

/*
material = new ShaderMaterial({

uniforms: ShaderLib.depth.uniforms,
vertexShader: ShaderLib.depth.vertexShader,
fragmentShader: ShaderLib.depth.fragmentShader			
*/

}