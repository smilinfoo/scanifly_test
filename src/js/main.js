import Scanify_Test from './Scanify_Test';

let canvas_holder = document.getElementById('container');
let app = new Scanify_Test();

app.init(canvas_holder);
app.start();